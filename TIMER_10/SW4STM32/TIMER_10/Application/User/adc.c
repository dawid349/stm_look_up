


#include "main.h"

volatile uint16_t voltage[200]={0};
volatile uint16_t voltage_average=0;
volatile uint16_t index_voltage=0;
extern volatile float Delta;
volatile float Delta_1=0;


void adc_delta_measure(ADC_HandleTypeDef* adc1)
{
	 if (HAL_ADC_PollForConversion(adc1, 10) == HAL_OK) {//czekamy na pomiar
		 	  voltage[index_voltage] = HAL_ADC_GetValue(adc1); //mierzymy
		 	  index_voltage++;
		 	  	  if(index_voltage>200)	//uwzgl�dniamy 200 ostatnich pr�bke do oblicze�
		 	  	  	  {
		 	  		  	  index_voltage=0;
		 	  		  	  int sum=0;
		 	  		  	  for(uint8_t i=0;i<200;i++)
		 	  		  	  {
		 	  		  		  sum=sum+voltage[i];
		 	  		  	  }
		 	  		  	  voltage_average=sum/200; //u�redniamy wynik
		 	  		  	  Delta_1=voltage_average*0.012+0.976; //przeskalowanie delty w przedziale (1-50)
		 	  		  	  Delta_1=((int)(Delta_1*10))/10.0F;//zaokr�glanie do jednej liczby po przecinku
		 	  		  	  Delta=Delta_1;					//aby unikn�c oscylacji wyniku w trakcie generacji sygna�u
		 	  	  	  }

		 	  HAL_ADC_Start(adc1); //w��czenie kolejnej pojedynczej konwersji

	 }
}









